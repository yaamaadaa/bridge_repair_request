Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  root "home#index"
  get "home/about"
  namespace :api, { format: "json" } do
    namespace :v1 do
      resources :users, only: [:index, :show, :create, :update, :destroy]
      resources :reports, only: [:index, :show, :create, :update, :destroy]
      resources :comments, only: [:index, :show, :create, :update]
    end
  end
end
