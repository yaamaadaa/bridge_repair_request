require "rails_helper"

RSpec.describe "home_system", type: :system do
  scenario "トップページの表示について" do
    visit root_path
    expect(current_path).to eq root_path
    expect(page).to have_title "橋梁補修リクエスト"
  end
end
