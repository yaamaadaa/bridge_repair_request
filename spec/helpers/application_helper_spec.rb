require "rails_helper"

RSpec.describe "ApplicationHelper", type: :helper do
  describe "Application Title helpers" do
    include ApplicationHelper
    it { expect(full_title("test")).to eq "test - 橋梁補修リクエスト" }
    it { expect(full_title("")).to eq "橋梁補修リクエスト" }
    it { expect(full_title(nil)).to eq "橋梁補修リクエスト" }
  end
end
