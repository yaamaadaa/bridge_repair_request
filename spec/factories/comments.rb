FactoryBot.define do
  factory :comment, class: Comment do
    content { "MyString" }
    user { build(:user) }
    report { build(:report) }
  end
end
