FactoryBot.define do
  factory :user, class: User do
    name { "MyString" }
    nickname { "MyString" }
    email { "MyString" }
    uid { "aaa" }
  end
end
