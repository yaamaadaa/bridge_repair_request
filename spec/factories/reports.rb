FactoryBot.define do
  factory :report, class: Report do
    content { "Content1" }
    title { "Title1" }
    address_lat { 40.82686488318179 }
    address_lng { 140.73597909627597 }
    solve { 0 }
    user { build(:user) }
    comments do
      [
        FactoryBot.build(:comment, report: nil),
      ]
    end
  end
end
