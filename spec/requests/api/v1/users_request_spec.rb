require "rails_helper"

describe Api::V1::UsersController, type: :request do
  describe "GET /api/v1/users" do
    context "全てのユーザーを取得する" do
      let!(:user) { create_list(:user, 10) }

      before do
        get "/api/v1/users"
      end

      it "200レスポンスが返ってくる" do
        expect(response.status).to eq(200)
      end

      it "正しい数のユーザーの情報が帰ってくる" do
        json = JSON.parse(response.body)
        expect(json.length).to eq(10)
      end
    end

    context "特定のユーザーを取得する" do
      let!(:user) { create(:user, name: "foo", uid: "abc") }
      let!(:other_user) { create(:user, name: "bar", uid: "def") }

      before do
        get "/api/v1/users", params: { uid: "abc" }
      end

      it "200レスポンスが返ってくる" do
        expect(response.status).to eq(200)
      end

      it "指定ユーザーの情報だけ返ってくる" do
        json = JSON.parse(response.body)
        expect(json["name"]).to eq(user.name)
      end
    end
  end
end
