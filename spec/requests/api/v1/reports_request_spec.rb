require "rails_helper"

describe Api::V1::ReportsController, type: :request do
  describe "GET /api/v1/reports" do
    context "全てのレポートを取得する" do
      let!(:report) { create_list(:report, 10) }
      before do
        get "/api/v1/reports", params: { page: 1 }
      end

      it "200レスポンスが返ってくる" do
        expect(response.status).to eq(200)
      end

      it "1ページ分のレポートが返ってくる" do
        json = JSON.parse(response.body)
        expect(json["reports"].length).to eq(5)
      end
    end

    context "特定のレポートを取得する" do
      let!(:report) { create(:report, title: "foo", id: 1) }
      let!(:other_report) { create(:report, title: "bar", id: 2) }
      before do
        get "/api/v1/reports", params: { id: 1 }
      end

      it "200レスポンスが返ってくる" do
        expect(response.status).to eq(200)
      end

      it "指定レポートの情報だけ返ってくる" do
        json = JSON.parse(response.body)
        expect(json["title"]).to eq(report.title)
      end
    end
  end
end
