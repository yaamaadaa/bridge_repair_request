require "rails_helper"

describe Api::V1::CommentsController, type: :request do
  describe "GET /api/v1/comments" do
    let!(:report) { create(:report, title: "foo", id: 1) }
    let!(:other_report) { create(:report, title: "bar", id: 2) }

    context "リクエストが成功した場合" do
      before do
        get "/api/v1/comments", params: { report_id: 1 }
      end

      it "200レスポンスが返ってくる" do
        expect(response.status).to eq(200)
      end
    end
  end
end
