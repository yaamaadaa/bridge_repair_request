class AddAddressToReports < ActiveRecord::Migration[5.2]
  def change
    add_column :reports, :address_lat, :decimal, :precision => 17, :scale => 15
    add_column :reports, :address_lng, :decimal, :precision => 17, :scale => 14
    add_column :reports, :solve, :boolean, default: false, null: false
  end
end
