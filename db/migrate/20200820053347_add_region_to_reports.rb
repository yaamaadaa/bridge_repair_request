class AddRegionToReports < ActiveRecord::Migration[5.2]
  def change
    add_column :reports, :region, :string
  end
end
