class ChangeDatatypeSolveOfReports < ActiveRecord::Migration[5.2]
  def change
    change_column :reports, :solve, :integer
  end
end
