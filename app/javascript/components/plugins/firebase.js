import firebase from "firebase/app"
import "firebase/auth"
import store from "../store/store.js";
import axios from 'axios';

const config = {
  apiKey: "AIzaSyD2RRJk84LPPKgHBgRM8M-XbdvP07kNY3k",
  authDomain: "bridgerepairrequest.firebaseapp.com",
  databaseURL: "https://bridgerepairrequest.firebaseio.com",
  projectId: "bridgerepairrequest",
  storageBucket: "bridgerepairrequest.appspot.com",
  messagingSenderId: "549039951719",
  appId: "1:549039951719:web:c011efb2c91f2e71e569c2",
  measurementId: "G-BYB68PPG7H"
};

export default {
    init() {
        firebase.initializeApp(config);
        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION);
    },
    logout() {
        firebase.auth().signOut()
    },
    onAuth() {
      firebase.auth().onAuthStateChanged(async user => {
        if (user) {
            const { data } = await axios.get(`/api/v1/users?uid=${user.uid}`)
            store.commit("setUser", data)
        } else {
            store.commit("setUser", null)
        }
      });
    }
};
