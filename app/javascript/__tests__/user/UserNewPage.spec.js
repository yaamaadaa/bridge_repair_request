import { shallowMount, createLocalVue } from "@vue/test-utils";
import VueRouter from "vue-router";
import UserNewPage from "../../components/user/UserNewPage.vue";

const localVue = createLocalVue()
const router = new VueRouter()
localVue.use(VueRouter)
let wrapper

beforeEach(() => {
  wrapper = shallowMount(UserNewPage, {
    stubs: ["router-link", "router-view"]
  })
})

describe("UserNewPage.vue", () => {
  it("入力欄があるか?", () =>{
    expect(wrapper.contains("v-text-field")).toBe(true)
  })

  it("登録ボタンがあるか?", () =>{
    expect(wrapper.contains("v-btn")).toBe(true)
  })
})
