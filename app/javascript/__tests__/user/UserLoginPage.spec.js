import { shallowMount, createLocalVue } from "@vue/test-utils";
import VueRouter from "vue-router";
import UserLoginPage from "../../components/user/UserLoginPage.vue";

const localVue = createLocalVue()
const router = new VueRouter()
localVue.use(VueRouter)
let wrapper

beforeEach(() => {
  wrapper = shallowMount(UserLoginPage, {
    stubs: ["router-link", "router-view"]
  })
})

describe("UserLoginPage.vue", () => {
  it("入力欄があるか?", () =>{
    expect(wrapper.contains("v-text-field")).toBe(true)
  })

  it("ログインボタンがあるか?", () =>{
    expect(wrapper.contains("v-btn")).toBe(true)
  })
})
