class Api::V1::ReportsController < ApiController
  include Pagenation
  before_action :set_report, only: [:show, :update]

  rescue_from Exception, with: :render_status_500

  rescue_from ActiveRecord::RecordNotFound, with: :render_status_404

  def index
    if params[:id]
      @report = Report.find_by(id: params[:id])
      render json: @report
    elsif params[:solve]
      @reports = Report.where(solve: params[:solve]).page(params[:solve_page]).per(5).order("created_at DESC")
      total_pages = @reports.total_pages
      render json: @reports, meta: { solve_total_pages: total_pages }, adapter: :json
    else
      @reports = Report.all.page(params[:page]).per(5).includes(:user).order("created_at DESC")
      total_pages = @reports.total_pages
      render json: @reports, meta: { total_pages: total_pages }, adapter: :json
    end
  end

  def show
    render json: @report
  end

  def create
    report = Report.new(report_params)
    if report.save
      render json: report, status: :created
    else
      render json: { errors: report.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def update
    if @report.update_attributes(report_params)
      head :no_content
    else
      render json: { errors: @report.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def destroy
    report = Report.find(params[:id])
    if report.destroy
      render json: report
    end
  end

  private

    def set_report
      @report = Report.find(params[:id])
    end

    def report_params
      params.require(:report).permit(:title, :content, :user_id, :address_lat, :address_lng, :image, :solve, :region)
    end

    def render_status_404(exception)
      render json: { errors: [exception] }, status: 404
    end

    def render_status_500(exception)
      render json: { errors: [exception] }, status: 500
    end
end
