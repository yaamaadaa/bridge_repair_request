class Api::V1::CommentsController < ApplicationController
  def index
    if params[:report_id]
      @report = Report.find_by(id: params[:report_id])
      @comments = @report.comments
      render json: @comments
    end
  end

  def create
    @comment = Comment.new(comment_params)
    if @comment.save
      render json: @comment, status: :created
    else
      render json: { errors: @comment.errors.full_messages }, status: :unprocessable_entity
    end
  end

  private

    def comment_params
      params.require(:comment).permit(:content, :user_id, :report_id)
    end
end
