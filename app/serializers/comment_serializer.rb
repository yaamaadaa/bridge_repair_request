class CommentSerializer < ActiveModel::Serializer
  attributes :id,
             :content,
             :user_id,
             :username,
             :report_id,
             :created_at,
             :updated_at

  def username
    object.user.nickname
  end
end
