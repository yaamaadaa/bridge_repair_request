class UserSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :email,
             :nickname,
             :uid

  has_many :reports
end
