class ReportSerializer < ActiveModel::Serializer
  attributes :id,
             :title,
             :content,
             :user_id,
             :username,
             :address_lat,
             :address_lng,
             :image,
             :created_at,
             :solve,
             :region

  has_many :comments

  def username
    object.user.nickname
  end
end
