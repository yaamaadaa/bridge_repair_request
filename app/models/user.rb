class User < ApplicationRecord
  has_many :reports, dependent: :destroy
  has_many :comments, dependent: :destroy
  validates :name, presence: true
  validates :email, presence: true
  validates :nickname, presence: true
end
