class Report < ApplicationRecord
  belongs_to :user
  validates :title, presence: true
  validates :address_lat, presence: true
  validates :address_lng, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  mount_base64_uploader :image, ImageUploader
  has_many :comments, dependent: :destroy
end
